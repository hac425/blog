<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<!--引入wangEditor.css-->
<link rel="stylesheet" type="text/css"
	href="editor/dist/css/wangEditor.min.css">


<!--引入jquery和wangEditor.js-->
<!--注意：javascript必须放在body最后，否则可能会出现问题-->
<script type="text/javascript"
	src="editor/dist/js/lib/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="editor/dist/js/wangEditor.min.js"></script>

<head>
<meta http-equiv="Content-Type" content="text/html;">
<title>Insert title here</title>
</head>
<body>
	<div style="width: 100%">
		<!--用当前元素来控制高度-->
		<div id="div1" style="height: 400px; max-height: 500px;">
			<p>请输入内容...</p>
		</div>
	</div>


</body>


<!--这里引用jquery和wangEditor.js-->
<script type="text/javascript">
	var editor = new wangEditor('div1');
	editor.create();
</script>


</html>