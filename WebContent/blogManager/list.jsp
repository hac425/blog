<%@ page language="Java" import="java.util.*, com.blog.bean.ArticleBean"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	request.setCharacterEncoding("UTF-8");
%>


<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description"
	content="Creative - Bootstrap 3 Responsive Admin Template">
<meta name="author" content="GeeksLabs">
<meta name="keyword"
	content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
<link rel="shortcut icon" href="img/favicon.png">

<title>List Articles</title>

<!-- Bootstrap CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet"
	href="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- bootstrap theme -->
<link href="css/bootstrap-theme.css" rel="stylesheet">
<!--external css-->
<!-- font icon -->
<link href="css/elegant-icons-style.css" rel="stylesheet" />
<link href="css/font-awesome.min.css" rel="stylesheet" />
<!-- date picker -->

<!-- color picker -->

<!-- Custom styles -->
<link href="css/style.css" rel="stylesheet">
<link href="css/style-responsive.css" rel="stylesheet" />

<!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
<!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <script src="js/lte-ie7.js"></script>
    <![endif]-->

</head>

<body>

	<!-- container section start -->
	<section id="container" class="">
		<!--header start-->
		<header class="header dark-bg">
			<div class="toggle-nav">
				<div class="icon-reorder tooltips"
					data-original-title="Toggle Navigation" data-placement="bottom">
					<i class="icon_menu"></i>
				</div>
			</div>

			<!--logo start-->
			<a href="index.html" class="logo">Nice <span class="lite">Admin</span></a>
			<!--logo end-->
			<div class="top-nav notification-row">
				<ul class="nav pull-right top-menu">

					<!-- user login dropdown start-->
					<li class="dropdown"><a data-toggle="dropdown"
						class="dropdown-toggle" href="#"> <span class="profile-ava">
								<img alt="" src="img/avatar1_small.jpg">
						</span> <span class="username">${sessionScope.username}</span> <b
							class="caret"></b>
					</a>
						<ul class="dropdown-menu extended logout">
							<div class="log-arrow-up"></div>
							<li><a href="../Logout"><i class="icon_key_alt"></i> Log
									Out</a></li>
						</ul></li>
					<!-- user login dropdown end -->
				</ul>
				<!-- notificatoin dropdown end-->
		</header>
		<!--header end-->

		<!--sidebar start-->
		<aside>
			<div id="sidebar" class="nav-collapse ">
				<!-- sidebar menu start-->
				<ul class="sidebar-menu">
					<li class="active"><a class="" href="#"> <i
							class="icon_house_alt"></i> <span>^_^</span>
					</a></li>

					<li><a class="" href="blogManager/index.jsp"> <i
							class="icon_genius"></i> <span>写点啥</span>
					</a></li>
					<li><a class="" href="#"> <i class="icon_piechart"></i> <span>浏览文章</span>
					</a></li>
					<li><a class="" href="#"> <i class="icon_genius"></i> <span>修改文章</span>
					</a></li>
					<li><a class="" href="#"> <i class="icon_piechart"></i> <span>删除文章</span>
					</a></li>



				</ul>
				<!-- sidebar menu end-->
			</div>
		</aside>
		<!--sidebar end-->

		<!--main content start-->
		<section id="main-content">
			<section class="wrapper">

				<c:forEach items="${articles}" var="article">

					<div>
						<h2 style="margin-bottom: 5px; font-size: 24px; font-weight: bold">${article.title}</h2>
						<div>

							<%-- 	<p style="font-size: 15px; color: rgba(51, 51, 51, 0.65);">${article.ac_abstract}</p>
							<span class="glyphicon glyphicon-tag"
								style="color: rgb(255, 140, 60); font-size: 13px;"> Tag</span>
 --%>
							<p style="font-size: 15px; color: rgba(51, 51, 51, 0.65);">
								<span class="glyphicon glyphicon-tag"
									style="color: rgb(255, 140, 60); font-size: 10px;"> 概要</span><br>
								&nbsp;&nbsp;${article.ac_abstract}
							</p>


						</div>
						<br> <br>
						<hr>

						<div style="float: right;">
							<a class="btn" href="ShowArticleServelt?id=${article.id}">查看更多 »</a> 
							<a class="btn" href="EditArticle?id=${article.id}">编辑文章</a>
							<a class="btn" href="DeleteArticle?id=${article.id}">删除文章</a>

						</div>

					</div>
					<hr>



				</c:forEach>


			</section>
		</section>
		<!--main content end-->

	</section>
	<!-- container section end -->
	<!-- javascripts -->
	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>

	<!--    <script src="http://cdn.static.runoob.com/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
	<!-- nice scroll -->
	<script src="js/jquery.scrollTo.min.js"></script>
	<script src="js/jquery.nicescroll.js" type="text/javascript"></script>

	<!-- jquery ui -->
	<script src="js/jquery-ui-1.9.2.custom.min.js"></script>

	<!--custom checkbox & radio-->
	<script type="text/javascript" src="js/ga.js"></script>
	<!--custom switch-->
	<script src="js/bootstrap-switch.js"></script>
	<!--custom tagsinput-->
	<script src="js/jquery.tagsinput.js"></script>

	<!-- colorpicker -->

	<!-- bootstrap-wysiwyg -->
	<script src="js/jquery.hotkeys.js"></script>
	<script src="js/bootstrap-wysiwyg.js"></script>
	<script src="js/bootstrap-wysiwyg-custom.js"></script>

	<!-- custom form component script for this page-->
	<script src="js/form-component.js"></script>
	<!-- custome script for all page -->
	<script src="js/scripts.js"></script>
</html>

