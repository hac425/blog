package blog.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.blog.bean.ArticleBean;

/**
 * Servlet implementation class ServeltToJsp
 */
@WebServlet("/ServeltToJsp")
public class ServeltToJsp extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServeltToJsp() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<ArticleBean> list = new ArrayList<ArticleBean>();
		ArticleBean bean = new ArticleBean();
		bean.setContent("sss");
		list.add(bean);
		bean = new ArticleBean();
		bean.setContent("dddd");
		list.add(bean);
		bean = new ArticleBean();
		bean.setContent("kkkkkkkk");
		list.add(bean);
		request.setAttribute("list", list);
		RequestDispatcher rd = request.getRequestDispatcher("/data_recv_test.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
