package com.blog.service;

import java.util.List;

import com.blog.bean.ArticleBean;
import com.blog.dao.ArticleDao;
import com.blog.dao.UserDao;

public class BlogService {

	private UserDao userDao;
	private ArticleDao articleDao;

	public BlogService() {
		userDao = new UserDao();
		articleDao = new ArticleDao();
	}

	public UserDao getUserDao() {
		
		return userDao;
	}
	
	public boolean addArticle(int userId, String title, String ac_abstract,String content) {
		return articleDao.insert(userId, title, ac_abstract, content);
	}
	
	public List<ArticleBean> findAllArticle(){
		return articleDao.getAllArticles();
	}
	
	public ArticleBean findArticleById(String id){
		return articleDao.selectOne(id);
	}
	
	public void deleteArticleById(String id){
		 articleDao.deleteOne(id);
	}
	
	public void modifyArticleById(String id, int userId, String title, String ac_abstract,String content) {
		 articleDao.update(id, userId, title, ac_abstract, content);
	}
}
