package com.blog.dao;

import java.io.InputStream;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.blog.bean.UserBean;

public class UserDao {

	private SqlSession session;
	private SqlSessionFactory sessionFactory;

	public UserDao() {
		// mybatis的配置文件
		String resource = "conf.xml";
		InputStream is = UserDao.class.getClassLoader().getResourceAsStream(resource);
		// 构建sqlSession的工厂
		sessionFactory = new SqlSessionFactoryBuilder().build(is);

	}

	public UserBean login(String username,String password) {
		boolean ret = false;
		
		UserBean userInfo = new UserBean();
		userInfo.setUsername(username);
		userInfo.setPassword(password);
		
		session = sessionFactory.openSession();
		String statement = "com.blog.dao.userMapper.login";// 映射sql的标识字符串
		// 执行查询返回一个唯一user对象的sql
		UserBean user = session.selectOne(statement, userInfo);
		session.close();
		if(user != null){
			ret = true;
		}
		
		
		return user;
	}

}
