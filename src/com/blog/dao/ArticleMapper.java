package com.blog.dao;

import java.util.List;

import com.blog.bean.ArticleBean;

public interface ArticleMapper {
	
	public List<ArticleBean> getAllArticles();
}
