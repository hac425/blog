package com.blog.dao;

import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.blog.bean.ArticleBean;

public class ArticleDao implements ArticleMapper {

	private SqlSession session;

	private SqlSessionFactory sessionFactory;

	public ArticleDao() {
		// mybatis的配置文件
		String resource = "conf.xml";
		InputStream is = ArticleDao.class.getClassLoader().getResourceAsStream(resource);
		// 构建sqlSession的工厂
		sessionFactory = new SqlSessionFactoryBuilder().build(is);

	}

	@Override
	public List<ArticleBean> getAllArticles() {
		session = sessionFactory.openSession();
		String statement = "com.blog.dao.ArticleMapper.getAllArticles";// 映射sql的标识字符串
		List<ArticleBean> articles = session.selectList(statement);
		session.close();
		return articles;
	}

	public ArticleBean selectOne(String id) {

		session = sessionFactory.openSession();
		String statement = "com.blog.dao.ArticleMapper.selectOne";// 映射sql的标识字符串
		ArticleBean article = (ArticleBean) session.selectOne(statement, Integer.parseInt(id));
		session.close();
		return article;
	}

	public void deleteOne(String id) {

		session = sessionFactory.openSession();
		String statement = "com.blog.dao.ArticleMapper.deleteById";// 映射sql的标识字符串
		session.delete(statement, Integer.parseInt(id));
		session.close();
	}

	public boolean insert(int userId, String title, String ac_abstract, String content) {

		ArticleBean articleBean = new ArticleBean();
		articleBean.setAc_abstract(ac_abstract);
		articleBean.setContent(content);
		articleBean.setTitle(title);
		articleBean.setUserId(userId);

		session = sessionFactory.openSession();
		String statement = "com.blog.dao.ArticleMapper.addArticle";// 映射sql的标识字符串
		try {
			session.insert(statement, articleBean);
		} catch (Exception e) {
			System.err.println(e);
			return false;
		} finally {
			session.close();
		}

		return true;
	}

	public boolean update(String id, int userId, String title, String ac_abstract, String content) {

		ArticleBean articleBean = new ArticleBean();
		articleBean.setAc_abstract(ac_abstract);
		articleBean.setContent(content);
		articleBean.setTitle(title);
		articleBean.setUserId(userId);
		articleBean.setId(Integer.parseInt(id));

		session = sessionFactory.openSession();
		String statement = "com.blog.dao.ArticleMapper.updateArticle";// 映射sql的标识字符串
		
		
		
		
		try {
			session.update(statement, articleBean);
		} catch (Exception e) {
			System.err.println(e);
			System.out.println("sss");
			return false;
		} finally {
			session.close();
		}

		return true;
	}
	
}
