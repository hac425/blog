package com.blog.bean;

/*
create database Blog;
use Blog;
CREATE TABLE users(
    id INT PRIMARY KEY AUTO_INCREMENT, 
    ownerId VARCHAR(20), 
    password VARCHAR(32) 
    );
INSERT INTO users(username, password) VALUES('admin', 'admin');
*/

public class UserBean {
	private String username;
	private String password;
	private int id;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Override
	public String toString() {
		
		return password + "@@" + username;
		
	}	
	
	

}
