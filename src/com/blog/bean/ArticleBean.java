package com.blog.bean;

public class ArticleBean {

	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	private int userId;
	private String content;
	private String ac_abstract = "";
	private String title;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAc_abstract() {
		return ac_abstract;
	}

	public void setAc_abstract(String ac_abstract) {
		this.ac_abstract = ac_abstract;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
