package com.blog.servelt;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.blog.service.BlogService;

/**
 * Servlet implementation class AddArticleServelt
 */
@WebServlet("/AddArticle")
public class AddArticleServelt extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddArticleServelt() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect(request.getContextPath()+ "/blogManager/index.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		BlogService blogService = new BlogService();
		int userId = (int) request.getSession().getAttribute("userId");
		String title = request.getParameter("title");
		String content = request.getParameter("content");
		String ac_abstract = request.getParameter("ac_abstract");
		if(blogService.addArticle(userId, title, ac_abstract, content)){
			doGet(request, response);
		}else{
			response.getWriter().append("insert error");
		}
		
	
	}

}
